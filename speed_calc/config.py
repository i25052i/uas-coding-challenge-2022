"""
Contains some configuration settings for the Speed Calculator.
"""

#GPS provider information
GPS_PROVIDER_URL = "http://127.0.0.1:8000"
GPS_REFRESH_DELAY = 1

#speed calculator options
SELECT_CURRENT = True #true for current speed, false for average
KEEP_LAST = -1 #how many waypoints to track at once
DELAY = -1 #delay between requests to GPS provider
