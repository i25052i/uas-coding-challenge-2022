"""
Tests sending requests to the GPS provider.
"""

import time
import requests

from config import GPS_PROVIDER_URL
from conversion import convert_gps_to_utm

initial = time.monotonic_ns()

while (time.monotonic_ns() - initial) < 5 * 1000000000:
    #send a request to GPS coordinate provider
    response = requests.get(GPS_PROVIDER_URL, timeout=0.5)

    #parse response
    lat, long = response.text.split(' ', 1)
    lat = float(lat.strip())
    long = float(long.strip())

    #print(lat, long, (time.monotonic_ns() - initial) / 1000000000)

    #convert to utm
    current_utm = convert_gps_to_utm(lat, long)

    print(current_utm, (time.monotonic_ns() - initial) / 1000000000)

    #delay
    time.sleep(0.25)
