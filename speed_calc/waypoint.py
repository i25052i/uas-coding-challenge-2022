"""
Implements the Waypoint and WaypointTracker classes to facilitate recording the
movement history of an object.

Waypoint objects hold the positional data of the object for a certain point in
time.
WaypointTrackers provide useful functionality to store and access a set of
recent waypoints.
"""

from collections import deque

class Waypoint():
    """
    A Waypoint captures the history of an object's movement by representing the
    position of the object at a certain point in time, and a displacement
    field that can be used to record the displacement between this and the
    previous waypoint.
    The position is recorded in UTM coordinates, while the timestamps are
    relative to other waypoints. The distance is recorded in meters.
    """

    def __init__(self, current_time, utm_coords, displacement):
        """
        Parameters:
        current_time - timestamp for this Waypoint
        utm_coords - UTM Coordinates for this Waypoint
        displacement - displacement for this Waypoint

        Constructs a new Waypoint object.
        """
        self._timestamp = current_time
        self._coordinates = utm_coords
        self._displacement = displacement

    def get_time(self):
        """
        Returns the timestamp associated with the waypoint.
        """
        return self._timestamp

    def get_coordinates(self):
        """
        Returns the UTM coordinates associated with the waypoint.
        """
        return self._coordinates

    def get_displacement(self):
        """
        Returns the displacement associated with the waypoint.
        """
        return self._displacement

class WaypointTracker():
    """
    A WaypointTracker is a helpful container for Waypoints that stores a
    certain number of the most recent Waypoints in a queue-like structure,
    and provides an interface to add and inspect stored Waypoints.
    """

    def __init__(self, keep_last=5):
        """
        Parameters:
        keep_last - the maximum number of waypoints that can be stored at one
                    time, default 5

        Constructs a new, empty WaypointTracker.
        """
        self._waypoints = deque([], keep_last)

    def add_waypoint(self, waypoint):
        """
        Parameters:
        waypoint - Waypoint to add

        Inserts a Waypoint into the container. If the number of Waypoints being
        stored is equal to keep_last before insertion, this operation will
        discard the oldest waypoint in storage.
        """
        self._waypoints.append(waypoint)

    def last_waypoint(self):
        """
        Peeks at the most recently added waypoint.

        Returns False if the container is empty.
        """
        if len(self._waypoints) > 0:
            return self._waypoints[-1]

        return False

    def list_waypoints(self):
        """
        Returns a list of all waypoints currently being stored.
        """
        return list(self._waypoints)
