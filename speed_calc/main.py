"""
speed_calc/main.py contains the main implementation of the speed calculator.

Instantiate a new SpeedCalculator object and call the run_forever() method to
start it. It will continually request coordinates from the GPS Provider,
calculate the speed (the user can specify whether this is current or average),
and report this information to the terminal.

Information such as the URL of the GPS provider and the given refresh delay of
the GPS is located in a config.py file.
"""

import time
import requests

from config import *
from conversion import convert_gps_to_utm, distance
from waypoint import Waypoint, WaypointTracker

class SpeedCalculator():
    """
    The SpeedCalculator class implements the functionality for receiving GPS
    coordinates and calculating the speed.
    Once the run_forever() method is called, the SpeedCalculator requests
    GPS coordinates from the provider once every delay seconds. It records
    the last keep_last instances of positional data, using this to calculate
    either average or current speed as selected by the user. The speed data
    is written to the terminal.

    The execution loop of a SpeedCalculator is completely decoupled from its
    GPS coordinate provider. Thus, it requests updated coordinates according
    to its own delay timer, rather than the refresh rate of the GPS provider.

    This is not a problem for average speed calculations, as this means the
    reported figure will simply update every delay seconds, jumping whenever
    the GPS reports new coordinates and averaging down until the next
    coordinates come in.

    However, this does affect current speed calculations. The instantaneous
    speed of the object is estimated using only the most recent keep_last
    waypoints. Since a waypoint is requested once every delay seconds, this
    means the estimation is based off of the last (keep_last * delay) seconds.

    The ratio between this sampled interval and the GPS's refresh rate
    determines the 'smoothness' of the reported speed.
    If the sample ratio is > 2, the speed is accumulated and 'smoothed' over
    multiple recent GPS coordinates. This is usually desired behaviour.
    If the ratio is < 1, the sampled region is very narrow compared to the
    GPS refresh rate, and calculations will percieve the object as moving in
    short bursts every time the coordinates update, rather than continuously
    moving.

    Users should select keep_last and delay to fit their needs, accounting for
    the GPS's refresh rate and the movement patterns of an object (sometimes
    an object really does move in short bursts, although optimally those bursts
    would have a duration longer than the refresh rate of the GPS.)
    """
    def __init__(self, select_current=False, keep_last=15, delay=0.25):
        """
        Parameters:
        select_current - True to report current speed, False to report average.
        keep_last - set the number of the most recent waypoints to track.
        delay - delay between subsequent queries to the GPS coordinate provider.

        Creates a new SpeedCalculator object.
        """
        #configuration
        self._speed_type = select_current
        self._keep_last = keep_last
        self._delay = delay
        #state
        self._wp_tracker = WaypointTracker(self._keep_last)
        self._initial_time = 0
        self._total_distance = 0

        #warning message
        if select_current and keep_last * delay < GPS_REFRESH_DELAY:
            print(f"Warning: this speed calculator has been configured to \
track waypoints in the last {keep_last * delay} second(s).\n\
This interval is less than the GPS provider's update rate of \
once every {GPS_REFRESH_DELAY} second(s).\n\
The reported current speed of the object may fall to 0m/s in \
between updates, because the calculator percieves the object \
as moving in short bursts every {GPS_REFRESH_DELAY} second(s), \
rather than smooth movement.\n\
To fix this, increase keep_last and or delay such that (keep_last * \
delay) is at least {GPS_REFRESH_DELAY}.\n")

    def _get_coords(self):
        """
        Retrieves a set of coordinates from the GPS provider, then converts the
        coordinates to UTM and calculates displacement. Creates a new Waypoint
        with this set of position/time data and adds it to the tracker.
        """
        #send a request to GPS coordinate provider
        response = requests.get(GPS_PROVIDER_URL, timeout=1)

        #record time immediately after response
        current_time = time.monotonic_ns() - self._initial_time

        #parse response
        lat, long = response.text.split(' ', 1)
        lat = float(lat.strip())
        long = float(long.strip())

        #convert to utm
        current_utm = convert_gps_to_utm(lat, long)

        #calculate displacement from last waypoint
        prev_wp_coords = self._wp_tracker.last_waypoint().get_coordinates()
        displacement = distance(prev_wp_coords, current_utm)

        #increment total distance
        self._total_distance += displacement

        #create a waypoint with position/time data
        waypoint = Waypoint(current_time, current_utm, displacement)

        #insert new waypoint into tracker
        self._wp_tracker.add_waypoint(waypoint)


    def _report_speed(self):
        """
        Calculates and reports the speed of the object to the terminal.
        Depending on input parameters, can calculate average or current speed
        of the object.
        Average speed is calculated as the distance traveled since start over
        the elapsed time.
        The current speed is estimated using the most recent waypoints that
        are currently being stored.
        """
        if self._speed_type: #current speed
            #retrieve a list of current waypoints
            waypoints = self._wp_tracker.list_waypoints()

            #calculate total distance over the interval
            position_diff = sum(waypoints[i].get_displacement() for i in range(1, len(waypoints)))
            #calculate time difference over the interval
            time_diff = waypoints[-1].get_time() - waypoints[0].get_time() #in nanoseconds

            #skip reporting if time_diff is 0
            if time_diff == 0:
                return

            #estimate instantaneous speed
            current_speed = position_diff / time_diff * 1000000000
            time_diff /= 1000000000 #convert to seconds

            #report to terminal
            print(f"Current speed: {current_speed:6.2f}m/s | Travelled {position_diff:6.2f}m over the last {time_diff:4.2f}s")
        else:
            #calculate elapsed_time
            elapsed_time = (time.monotonic_ns() - self._initial_time) #time in nanoseconds

            #skip reporting if elapsed_time is 0
            if elapsed_time == 0:
                return

            #calculate average speed
            avg_speed = self._total_distance / elapsed_time * 1000000000
            elapsed_time /= 1000000000 #convert to seconds

            #report to terminal
            print(f"Average speed: {avg_speed:6.2f}m/s | Time elapsed: {elapsed_time:6.2f}s, Total distance: {self._total_distance:7.2f}m")

    def run_forever(self):
        """
        Resets this SpeedCalculator object into a known initial internal state,
        then retrieves the starting coordinates and creates the initial
        Waypoint. Then, enters the main execution loop, retrieving a new set of
        coordinates and calculating and reporting the speed to the terminal.
        """
        #reset internal state
        self._wp_tracker = WaypointTracker(self._keep_last)
        self._total_distance = 0

        #get initial coordinates
        response = requests.get(GPS_PROVIDER_URL, timeout=0.5)

        #record time immediately after response as initial time
        self._initial_time = time.monotonic_ns()

        #parse response
        lat, long = response.text.split(' ', 1)
        lat = float(lat.strip())
        long = float(long.strip())

        #convert to utm
        current_utm = convert_gps_to_utm(lat, long)

        #create a waypoint with position/time data
        waypoint = Waypoint(0, current_utm, 0)

        #insert new waypoint into tracker
        self._wp_tracker.add_waypoint(waypoint)

        #loop
        while True:
            #retrieve the next set of coordinates
            self._get_coords()

            #calculate and report the speed
            self._report_speed()

            #wait before looping again
            time.sleep(self._delay)




#debug
if __name__ == "__main__":
    sc = SpeedCalculator(select_current=SELECT_CURRENT,
                         keep_last=(KEEP_LAST if KEEP_LAST > 0 else 15),
                         delay=(DELAY if DELAY > 0 else 0.25))

    sc.run_forever()
