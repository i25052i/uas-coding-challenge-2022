"""
gps_provider/main.py contains the implementation of the GPS provider server,
which serves the current set of coordinates in response to HTTP requests.
"""

import http.server
import socketserver

from config import INPUT_DATA_FILENAME, HOSTNAME, PORT
from coordinate_reader import CoordinateReader

#instantiate CoordinateReader
reader = CoordinateReader(INPUT_DATA_FILENAME)

#define request handler
class Handler(http.server.BaseHTTPRequestHandler):
    """
    Subclassed to handle HTTP requests that arrive at the server.
    """
    def do_GET(self):
        """
        Serves the current coordinates.
        """
        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write((reader.get_coords()).encode("utf-8")) #get coords from reader

#run the server
with socketserver.TCPServer((HOSTNAME, PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
