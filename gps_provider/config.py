"""
Contains some configuration settings for the GPS Coordinate Provider.
"""

#path to input data file
INPUT_DATA_FILENAME = "../data/missionwaypoints.txt"

#server configuration
HOSTNAME = "127.0.0.1"
PORT = 8000
