"""
coordinate_reader.py defines class CoordinateReader, which handles the loading
of coordinates from the input data file and presenting the correct set of
coordinates to the server.
"""

import time

class CoordinateReader():
    """
    A CoordinateReader object is responsible for loading the set of waypoints
    from the input text file. This occurs during object initialization.
    After the first request is processed, it will update the set of coordinates
    to send every second. Calls to get_coords will return the appropriate set
    of coordinates for the point in time the request was made.
    """
    def __init__(self, input_datafile):
        """
        Parameter:
        input_datafile - path to input file to use
        """
        self._initial_time = -1
        self._waypoints = []

        #read in waypoints from file
        with open(input_datafile, "rt", encoding="utf-8") as wp_file:
            for line in wp_file.readlines():
                self._waypoints.append(line.strip().replace('\t',' '))

    def get_coords(self):
        """
        Returns the appropriate set of coordinates for the point in time this
        function was called.
        On the first call to this function in the object's lifetime, returns
        the first set of coordinates and begins updating the coordinates every
        second. After enough time has elapsed such that the number of seconds
        that have passed exceeds the number of waypoints, further calls to
        get_coords will continue to return the last waypoint in the dataset.
        """
        #check if this is the first request for coordinates
        if self._initial_time == -1:
            #set initial time
            self._initial_time = time.monotonic_ns()

            #return first set of coordinates
            return self._waypoints[0]

        #calculate the timing of request
        request_time = (time.monotonic_ns() - self._initial_time) // 1000000000

        #requests far in the future default to last coordinates
        index = min(request_time, len(self._waypoints) - 1)

        return self._waypoints[index]
