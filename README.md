# UAS-Coding-Challenge-2022

## GPS Speed Challenge (B-GPS-0)

The goal of this challenge is to create an application that computes the speed of an object given a series of GPS coordinates.

---

## General Specification

This project will be implemented in Python3.

The application will consist of two main components: a program that serves coordinates (1) and a program that recieves them and calculates the speed (2). Furthermore, IPC (3) will be implemented with HTTP requests, and for the purposes of this challenge, input GPS data will be provided by a text file (4) and output data will be printed to the terminal (5). See the diagram below:

<p align="center"><img src="figures/component_diagram.png" width="75%" height="75%" title="component diagram"></p>

Components in the project should meet the following requirements regardless of implementation:

### GPS Coordinate Provider (1)

- Reads in GPS coordinates from an input text file.
- Updates the set of coordinates that it serves every second.
- Only starts updating the coordinates once the first request from the speed calculator has been handled.
- Once it runs out of coordinate data, it continues to serve the last waypoint in the dataset.

### Speed Calculator (2)

- Takes GPS coordinates of an object as input.
- Outputs the current speed of the object to the terminal in meters per second.
- Can be set to display average speed (over the entire series) or current speed.

### IPC (3)

- A basic client-server model will be used, with the GPS coordinate provider as the server and the speed calculator as the client.
- At any time, the speed calculator can make a GET request to the server to obtain the current position the GPS coordinate provider has on hand.

Note: this definition allows the speed calculator can make any number of requests within a given second, so it could make multiple requests within a second (and receive the same coordinates in all responses). It could even make no requests in a given second and miss a set of coordinates entirely. See `t = 1` and `t = 2` in the diagram below.

This decision was made to decouple the design of the speed calculator from the refresh rate of the GPS provider. As long as the speed calculator tracks its own time and makes requests at a reasonably fast frequency compared to the rate at which the GPS coordinates are updated, it should be compatible with a range of refresh rates. More information about the interaction between the GPS refresh rate and the speed calculator's polling rate can be found in the docstring for class `SpeedCalculator` defined in `speed_calc/main.py`.

<p align="center"><img src="figures/ipc_diagram.png" width="75%" height="75%" title="IPC diagram"></p>

### Input Data File (4)

- In .txt format.
- One set of GPS coordinates on every line.
- Coordinates are given in degrees with latitude followed by longitiude.
- Coordinates are meant to be spaced 1 second apart.

### Terminal (5)

- Output data is written to the terminal by the speed calculator.

---

## Configuration

### GPS Provider
The configuration file is located in `gps_provider/config.py`. It contains the following settings:
| Value | Description |
| ----- | ----------- |
| `INPUT_DATA_FILENAME` | path to the text file containing the desired coordinate data |
| `HOSTNAME` | server hostname |
| `PORT` | server port

### Speed Calculator
The configuration file is located in `speed_calc/config.py`. It contains the following settings:
| Value | Description |
| ----- | ----------- |
| `GPS_PROVIDER_URL` | URL for the GPS provider |
| `GPS_REFRESH_DELAY` | the time (in seconds) it takes for the GPS provider to update its coordinates. Used for a warning message - the program will function correctly even if this is set incorrectly.|
| `SELECT_CURRENT` | set this to `True` to display the current speed or `False` to display the average speed |
| `KEEP_LAST` | how many datapoints the calculator should track (see `SpeedCalculator` docstring in `speed_calc/main.py` |
| `DELAY` | how long the calculator should wait between requests to the GPS coordinate provider |

---

For implementation-specific details, inspect the code itself.

This program was tested after completion by examining its output with a variety of input datasets.
